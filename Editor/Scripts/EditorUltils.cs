using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

public static class EditorUltils
{
    public static bool IsControl()
    {
        if (Event.current.control)
        {
            HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
            return true;
        }
        return false;
    }

    public static bool IsShift()
    {
        if (Event.current.shift)
        {
            HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
            return true;
        }
        return false;
    }

    public static Vector3 GetPoint()
    {
        Vector3 point = Event.current.mousePosition;
        Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
        var obj = HandleUtility.RaySnap(ray);
        if (obj != null && obj is RaycastHit rayHit)
        {
            point = rayHit.point;
        }
        return point;
    }

    public static void AddPoint(ref List<Vector3> Points)
    {
        Debug.Log("Add Point");
        var newPoint = GetPoint();

        for (int i = 0; i < Points.Count - 1; i++)
        {
            float d = DistancePointToLine(newPoint, Points[i], Points[i + 1], out Vector3 closest);
            if (d <= EditorDraw.DistanceSelectLine)
            {
                Points.Insert(i + 1, newPoint); return;
            }
        }
        Points.Add(newPoint);
    }
    public static float DistancePointToLine(
     Vector3 pt, Vector3 p1, Vector3 p2, out Vector3 closest)
    {
        float dx = p2.x - p1.x;
        float dz = p2.z - p1.z;
        if ((dx == 0) && (dz == 0))
        {
            // It's a point not a line segment.
            closest = p1;
            dx = pt.x - p1.x;
            dz = pt.z - p1.z;
            return Mathf.Sqrt(dx * dx + dz * dz);
        }

        // Calculate the t that minimizes the distance.
        float t = ((pt.x - p1.x) * dx + (pt.z - p1.z) * dz) /
            (dx * dx + dz * dz);

        // See if this represents one of the segment's
        // end points or a point in the middle.
        if (t < 0)
        {
            closest = new Vector3(p1.x, 0, p1.z);
            dx = pt.x - p1.x;
            dz = pt.z - p1.z;
        }
        else if (t > 1)
        {
            closest = new Vector3(p2.x, 0, p2.z);
            dx = pt.x - p2.x;
            dz = pt.z - p2.z;
        }
        else
        {
            closest = new Vector3(p1.x + t * dx, 0, p1.z + t * dz);
            dx = pt.x - closest.x;
            dz = pt.z - closest.z;
        }

        return Mathf.Sqrt(dx * dx + dz * dz);
    }
    public static void UpdatePoints(List<Vector3> Points)
    {
        for (int i = 0; i < Points.Count; i++)
        {
            Vector3 point = Points[i];

            EditorGUI.BeginChangeCheck();
            point = Handles.FreeMoveHandle(point, 1, Vector3.zero, Handles.CircleHandleCap);
            if (EditorGUI.EndChangeCheck())
            {
                Points[i] = GetPoint();
            }
        }
    }
    public static bool IsMouseDown()
    {
        return Event.current.type == EventType.MouseDown;
    }
}
