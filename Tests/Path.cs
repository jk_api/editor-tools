using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteAlways]
public class Path : MonoBehaviour
{
    public bool IsClosePath;
    public List<Vector3> Points;

    public void AddPoint(Vector3 point)
    {
        if (Points == null)
        {
            Points = new List<Vector3>();
        }
        Points.Add(point);
    }

    public float PolygonArea()
    {
        int num_points = Points.Count;
        Vector3[] pts = new Vector3[num_points + 1];
        Points.CopyTo(pts, 0);
        pts[num_points] = Points[0];

        // Get the areas.
        float area = 0;
        for (int i = 0; i < num_points; i++)
        {
            area +=
                (pts[i + 1].x - pts[i].x) *
                (pts[i + 1].z + pts[i].z) / 2;
        }

        // Return the result.
        return Mathf.Abs(area);
    }
}
