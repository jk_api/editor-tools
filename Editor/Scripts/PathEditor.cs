﻿using System.Collections.Generic;
using System.Text;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Path), true)]
public class PathEditor : GameEditor
{
    bool IsShowIndex;
    Path Target;
    float length = 0;
    float area = 0;
    private void OnEnable()
    {
        Target = (Path)target;
    }
    public override void Options()
    {
        GUILayout.Label("Chu vi: " + length.ToString("0.00"), EditorDraw.Skin.label);
        GUILayout.Label("Diện tích: " + area.ToString("0.00"), EditorDraw.Skin.label);
        GUILayout.Label("Diện tích: " + Target.PolygonArea().ToString("0.00"), EditorDraw.Skin.label);
        IsShowIndex = GUILayout.Toggle(IsShowIndex, "Show Index", EditorDraw.Skin.toggle);
        Target.IsClosePath = GUILayout.Toggle(Target.IsClosePath, nameof(Target.IsClosePath), EditorDraw.Skin.toggle);
    }
    public override void OnSceneGUI()
    {
        EditorUltils.UpdatePoints(Target.Points);

        if (EditorUltils.IsMouseDown())
        {
            //Add Point
            if (EditorUltils.IsControl())
            {
                EditorUltils.AddPoint(ref Target.Points);
            }
        }
        EditorDraw.DrawPoints(ref Target.Points, Color.red, IsShowIndex);
        length = EditorDraw.DrawLines(Target.Points, Color.blue, Target.IsClosePath);
        CalculatorArea();
        base.OnSceneGUI();
    }

    void CalculatorArea()
    {
        float minX = float.MaxValue;
        float maxX = float.MinValue;
        float minZ = float.MaxValue;
        int indexMinX = int.MaxValue, indexMaxX = int.MinValue;
        for (int i = 0; i < Target.Points.Count; i++)
        {
            Vector3 point = Target.Points[i];
            if (point.x <= minX)
            {
                minX = point.x;
                indexMinX = i;
            }
            if (point.x >= maxX)
            {
                maxX = point.x;
                indexMaxX = i;
            }
            if (point.z <= minZ)
            {
                minZ = point.z;
            }
        }
        //Debug.Log("Min x:" + indexMinX);
        //Debug.Log("Max x:" + indexMaxX);
        float dZ = minZ - 2;
        float area1 = AreaPolygon(Target.Points, indexMinX, indexMaxX, dZ, -1);
        float area2 = AreaPolygon(Target.Points, indexMinX, indexMaxX, dZ, 1);
        area = Mathf.Abs(area1 - area2);
        //Debug.Log("Area 1:" + area1);
        //Debug.Log("Area 2:" + area2);
        Handles.color = Color.red;
        //int indexOutside = area1 < area2 ? -1 : 1;
        //int id = indexMinX;
        //int count = 0;
        //while (id <= indexMaxX)
        //{
        //    id = GetIndex(id, Target.Points.Count);
        //    Vector3 point = Target.Points[id];
        //    Handles.DrawDottedLine(point, new Vector3(point.x, 0, dZ), 0.2f);
        //    id = GetIndex(id + indexOutside, Target.Points.Count);
        //    count++;
        //    Debug.Assert(count < Target.Points.Count);
        //}
        // Show Lines
        Handles.DrawLine(new Vector3(minX, 0, dZ), new Vector3(maxX, 0, dZ));
        //Handles.DrawDottedLine(Target.Points[indexMinX], new Vector3(minX, 0, dZ), 0.2f);
        //Handles.DrawDottedLine(Target.Points[indexMaxX], new Vector3(maxX, 0, dZ), 0.2f);
        //area = Target.PolygonArea();

    }
    int GetIndex(int index, int count)
    {
        if (index >= count) return 0;
        if (index < 0) return count - 1;
        return index;
    }
    public float AreaPolygon(List<Vector3> Points, int indexMin, int indexMax, float dz, int sign)
    {
        float area = 0;
        int count = Points.Count;
        int index = indexMin;
        int i = 0;
        while (index != indexMax)
        {
            int nextIndex = GetIndex(index + sign, count);
            float z0 = Points[index].z - dz;
            float z1 = Points[nextIndex].z - dz;
            float h = Mathf.Abs(Points[index].x - Points[nextIndex].x);
            area += (z0 + z1) * h / 2f;
            index = nextIndex;
            i++;
            //Debug.Assert(i < Target.Points.Count);
        }

        return area;
    }

    string Message()
    {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.AppendLine("Ctrl + Click => Add Point");
        stringBuilder.AppendLine("Shift + Click => Remove Point");
        return stringBuilder.ToString();
    }

    public override void OnInspectorGUI()
    {
        EditorGUILayout.HelpBox(Message(), MessageType.Info);
        base.OnInspectorGUI();
        if (GUILayout.Button("OnClick"))
        {
            CalculatorArea();
        }
    }
}


