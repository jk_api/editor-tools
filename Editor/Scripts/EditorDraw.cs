﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class EditorDraw
{
    public const float DistanceArrow = 3;
    public const float RadiusButton = 1;
    public const float DistanceSelectLine = 0.5f;
    static GUISkin skin;
    public static GUISkin Skin
    {
        get
        {
            if (skin == null)
            {
                skin = new GUISkin();
                skin.label.alignment = TextAnchor.MiddleCenter;

            }
            skin.toggle.alignment = TextAnchor.MiddleRight;
            return skin;
        }
    }

    public static void DrawPoints(ref List<Vector3> Points, Color color, bool showIndex)
    {
        var point = EditorUltils.GetPoint();

        Debug.Assert(Points != null);

        for (int i = 0; i < Points.Count; ++i)
        {
            bool isSelect = Vector3.Distance(point, Points[i]) <= RadiusButton;
            Handles.color = isSelect ? Color.yellow : color;
            if (showIndex)
            {
                DrawText(i, Points[i], Vector3.up);
            }
            if (EditorUltils.IsShift() && Handles.Button(Points[i], Quaternion.identity, RadiusButton, RadiusButton, Handles.SphereHandleCap))
            {
                Handles.color = Color.yellow;
                Debug.Log(i);
                Points.RemoveAt(i);
                return;
            }
            else
            {
                if (!showIndex)
                    Handles.SphereHandleCap(0, Points[i], Quaternion.identity, 2, EventType.Repaint);
            }

        }
    }

    public static float DrawLines(List<Vector3> Points, Color color, bool isClosed)
    {
        Debug.Assert(Points != null);
        float Length = 0;
        if (Points.Count < 2) return Length;
        var point = EditorUltils.GetPoint();
        for (int i = 0; i < Points.Count - 1; i++)
        {
            bool selectLine = false;
            if (EditorUltils.IsControl())
            {
                float d = EditorUltils.DistancePointToLine(point, Points[i], Points[i + 1], out Vector3 closest);
                selectLine = d <= DistanceSelectLine;
            }
            Handles.color = selectLine ? Color.yellow : color;
            Length += DrawLine(Points[i], Points[i + 1]);
        }
        if (isClosed)
        {
            Length += DrawLine(Points[Points.Count - 1], Points[0]);
        }
        return Length;
    }
    static float DrawLine(Vector3 start, Vector3 end, bool isShowArrow = false)
    {
        Vector3 direct = end - start;
        Handles.DrawDottedLine(start, end, 0.2f);
        float distance = direct.magnitude;
        float percent = (Time.time % distance) / distance;
        Vector3 center = start + direct / 2;
        Vector3 position = GetPointInLine(start, end, percent);
        int countArrow = Mathf.CeilToInt(direct.magnitude / DistanceArrow);
        if (isShowArrow)
        {
            for (int i = 0; i < countArrow; i++)
            {
                DrawArrow(start + direct * (i / (float)countArrow), direct);
            }
            DrawArrow(position, direct);
        }
        DrawText(distance.ToString("0.00"), center);
        return distance;
    }

    static Vector3 GetPointInLine(Vector3 start, Vector3 end, float percent = 0.5f)
    {
        return start + (end - start) * percent;
    }

    static void DrawText(object text, Vector3 position, Vector3 offset = default(Vector3))
    {
        Handles.Label(position, text.ToString(), Skin.label);
    }

    static void DrawArrow(Vector3 position, Vector3 normal, float size = 0.5f)
    {
        //if (Time.time % 2 > 0.8f) return;
        //Handles.DrawLines(Arrow(position));     

        Handles.DrawPolyLine(Arrow(position, normal, size));
    }

    static void MatrixArray(ref Vector3[] points, Vector3 position, Vector3 normal)
    {

        Matrix4x4 matrix = Matrix4x4.TRS(position, Quaternion.LookRotation(normal, Vector3.up), Vector3.one);
        for (int i = 0; i < points.Length; i++)
        {
            points[i] = matrix.MultiplyPoint(points[i]);
        }
    }

    static Vector3[] Arrow(Vector3 position, Vector3 normal, float size)
    {
        Vector3[] rs = new Vector3[7];
        rs[0] = new Vector3(0, 0, 0.5f) * size;
        rs[1] = new Vector3(-0.5f, 0, -0.5f) * size;
        rs[2] = new Vector3(-0.25f, 0, -0.5f) * size;
        rs[3] = Vector3.zero;
        rs[4] = new Vector3(0.25f, 0, -0.5f) * size;
        rs[5] = new Vector3(0.5f, 0, -0.5f) * size;
        rs[6] = rs[0];
        MatrixArray(ref rs, position, normal);
        return rs;
    }
}
