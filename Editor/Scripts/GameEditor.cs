﻿using UnityEditor;
using UnityEngine;

public class GameEditor : Editor
{
    void ShowOption()
    {
        SceneView sceneView = SceneView.currentDrawingSceneView;
        Rect rect = new Rect(50, 30, 100, 600);
        GUILayout.BeginArea(rect, "Options");
        Options();
        GUILayout.EndArea();
    }

    public virtual void Options()
    {

    }
    public virtual void OnSceneGUI()
    {
        EditorApplication.QueuePlayerLoopUpdate();
        SceneView.RepaintAll();
        ShowOption();
    }
}

class EditorHelper
{
    [InitializeOnLoadMethod]
    private static void Init()
    {
        new EditorHelper();
    }

    EditorHelper()
    {
        SceneView.duringSceneGui += OnSceneGUI;
    }

    ~EditorHelper()
    {
        SceneView.duringSceneGui -= OnSceneGUI;
    }
    private static void OnSceneGUI(SceneView view)
    {

    }
}
